import { Component, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EventEmitter } from '@angular/core';
import { actorCreacionDTO, actorDTO } from '../Actores';

@Component({
  selector: 'app-formulario-actores',
  templateUrl: './formulario-actores.component.html',
  styleUrls: ['./formulario-actores.component.css']
})
export class FormularioActoresComponent implements OnInit {

  constructor(private formBuilder: FormBuilder) { }
  form:FormGroup;
  @Input()
  modelo: actorDTO;
  @Output()
  onSubmit: EventEmitter<actorCreacionDTO> = new EventEmitter<actorCreacionDTO>();
  ngOnInit(): void {
  this.form= this.formBuilder.group({
    nombre:[
      '', {
        validators:[Validators.required]
      }
    ],
    fechaNacimiento:'',
    foto:"",
    biblografia:""
  });
  if (this.modelo!== undefined){
    this.form.patchValue(this.modelo);
  }
  
  }
  archivoSeleccionado(file){
  this.form.get('foto').setValue(file);
  }
  Onsubmit(){
this.onSubmit.emit(this.form.value);
  }
  cambioMardown(texto: string){
    this.form.get('biblografia').setValue(texto);
  }

}
