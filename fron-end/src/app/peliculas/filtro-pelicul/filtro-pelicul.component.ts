import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-filtro-pelicul',
  templateUrl: './filtro-pelicul.component.html',
  styleUrls: ['./filtro-pelicul.component.css']
})
export class FiltroPeliculComponent implements OnInit {

  constructor(private formBuilder: FormBuilder,
     private location: Location, private activeRoute:ActivatedRoute
    ) { }
  form: FormGroup;
  generos = [
    { id: 1, nombre: "Drama" },
    { id: 2, nombre: "Accion" },
    { id: 3, nombre: "Terror" }
  ]
  peliculas = [
    {
      titulo: "Spider-Man: Far For home", enCines: false, proximosEstrenos: true,
      generos: [1, 2],
      poster: "https://assets.cinepolisklic.com/cmsklicia/movieimages/spider-man-sin-camino-a-casa-preventa/poster_resize_182X273.jpg"
    },
    {
      titulo: "Matrix", enCines: true, proximosEstrenos: false,
      generos: [1, 2],
      poster: "https://assets.cinepolisklic.com/cmsklicia/movieimages/coleccion-matrix-resurrecciones-material-adicional/poster_resize_182X273.jpg"
    },
    {
      titulo: "Dream", enCines: false, proximosEstrenos: true,
      generos: [1, 3],
      poster: "https://assets.cinepolisklic.com/cmsklicia/movieimages/scream-grita/poster_resize_182X273.jpg"
    }
  ]
  pelicularOriginales = this.peliculas;
  formularioOriginal = {
    titulo: '',
    generoId: 0,
    proximosEstrenos: false,
    enCines: false
  }
  ngOnInit(): void {
    this.form = this.formBuilder.group(this.formularioOriginal);
     this.leerValoresUrl();
     this.buscarPeliculas(this.form.value);
    this.form.valueChanges.subscribe(valores => {
      this.peliculas = this.pelicularOriginales;
      this.buscarPeliculas(valores);
      this.escribirParametrosBusquedaUrl();
    });
  }
private leerValoresUrl(){
 this.activeRoute.queryParams.subscribe((params=>{
   var objeto: any= {};

   if(params.titulo){
     objeto.titulo=params.titulo
   }
   if(params.generoId){
    objeto.generoId= Number(params.generoId);
  }
  if(params.proximosEstrenos){
    objeto.proximosEstrenos=params.proximosEstrenos;
  }
  if(params.enCines){
    objeto.enCines=params.enCines;
  }
  this.form.patchValue(objeto);
 }))
}

  private escribirParametrosBusquedaUrl(){
    var queryStrings=[];

    var valoresFormularios= this.form.value;

    if(valoresFormularios.titulo){
      queryStrings.push(`titulo=${valoresFormularios.titulo}`);
    }
    if(valoresFormularios.generoId !=0){
      queryStrings.push(`generoId=${valoresFormularios.generoId}`);
    }
    if(valoresFormularios.proximosEstrenos){
      queryStrings.push(`proximosEstrenos=${valoresFormularios.proximosEstrenos}`);
    }
    if(valoresFormularios.enCines){
      queryStrings.push(`enCines=${valoresFormularios.enCines}`);
    }
    this.location.replaceState('peliculas/Buscar', queryStrings.join('&'))
  }
  buscarPeliculas(valores: any) {
    if (valores.titulo) {
      this.peliculas = this.peliculas.filter(pelicula => pelicula.titulo.indexOf(valores.titulo) !== -1)
    }
    if (valores.generoId) {
      this.peliculas = this.peliculas.filter(pelicula => pelicula.generos.indexOf(valores.generoId) !== -1)
    }
    if (valores.proximosEstrenos) {
      this.peliculas = this.peliculas.filter(pelicula => pelicula.proximosEstrenos);
    }
    if (valores.enCines) {
      this.peliculas = this.peliculas.filter(pelicula => pelicula.enCines);
    }
  }
  limpiar() {
    this.form.patchValue(this.formularioOriginal);
  }

}
