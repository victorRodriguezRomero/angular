import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CrearActorComponent } from './actores/crear-actor/crear-actor.component';
import { EditarActorComponent } from './actores/editar-actor/editar-actor.component';
import { IndiceActoresComponent } from './actores/indice-actores/indice-actores.component';
import { CrearCineComponent } from './cines/crear-cine/crear-cine.component';
import { EditarCineComponent } from './cines/editar-cine/editar-cine.component';
import { IndiceCineComponent } from './cines/indice-cine/indice-cine.component';
import { CrearGeneroComponent } from './generos/crear-genero/crear-genero.component';
import { EditarGeneroComponent } from './generos/editar-genero/editar-genero.component';
import { IndiceGenerosComponent } from './generos/indice-generos/indice-generos.component';
import { LandinPageComponent } from './landin-page/landin-page.component';
import { CrearPeliculasComponent } from './peliculas/crear-peliculas/crear-peliculas.component';
import { EditarPeliculasComponent } from './peliculas/editar-peliculas/editar-peliculas.component';
import { FiltroPeliculComponent } from './peliculas/filtro-pelicul/filtro-pelicul.component';

const routes: Routes = [{
  path:"", component: LandinPageComponent
},
{
  path:"generos", component:IndiceGenerosComponent
},
{
  path:"generos/crear", component:CrearGeneroComponent
},
{
  path:"generos/editar/:id", component:EditarGeneroComponent
},
{
  path:"actores", component:IndiceActoresComponent
},
{
  path:"actores/crear", component:CrearActorComponent
},
{
  path:"actores/editar/:id", component:EditarActorComponent
},
{
  path:"cines", component:IndiceCineComponent
},
{
  path:"cines/crear", component:CrearCineComponent
},
{
  path:"cines/editar/:id", component:EditarCineComponent
},
{
  path:"peliculas/crear", component:CrearPeliculasComponent
},
{
  path:"peliculas/editar/:id", component:EditarPeliculasComponent
},
{path: "peliculas/buscar", component:FiltroPeliculComponent},
{
  path:"**", redirectTo: ''
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
