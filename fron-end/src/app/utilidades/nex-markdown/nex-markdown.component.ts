import { Component, OnInit, Output } from '@angular/core';
import {EventEmitter}  from '@angular/core';

@Component({
  selector: 'app-nex-markdown',
  templateUrl: './nex-markdown.component.html',
  styleUrls: ['./nex-markdown.component.css']
})
export class NexMarkdownComponent implements OnInit {
contenidoMarkdown=' ';
@Output()
changeMarkdown: EventEmitter<String>= new EventEmitter<string>();
  constructor() { }

  ngOnInit(): void {
  }
  inputTextArea(texto:string){
    this.contenidoMarkdown=texto;
    this.changeMarkdown.emit(texto);
  }
}
