import { createOutput } from '@angular/compiler/src/core';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';




@Component({
  selector: 'app-rating',
  templateUrl: './rating.component.html',
  styleUrls: ['./rating.component.css']
})
export class RatingComponent implements OnInit {
  @Input()
  MaximoRating=5;
  @Output()
  rated: EventEmitter<number> = new EventEmitter<number>();
  @Input()
  MaximoSeleccionado=0;
  MaximoRatingArreglo=[]
  votado=false;
  ratinganterior;
  constructor() { }


  ngOnInit(): void {
    this.MaximoRatingArreglo=Array(this.MaximoRating).fill(0);
  }
  manejarMouseEnter(index : number): void{
    this.MaximoSeleccionado=index+1;
  }
  manejarMouseLeave(){
    if(this.ratinganterior !==0){
      this.MaximoSeleccionado=this.ratinganterior;
    }else{
      this.MaximoSeleccionado=0;
    }
    
  }
  rate(index:number): void{
    this.MaximoSeleccionado=index+1;
    this.votado=true;
    this.ratinganterior=this.MaximoSeleccionado;
    this.rated.emit(this.MaximoSeleccionado);
  }
}
