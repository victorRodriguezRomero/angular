import { Component, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EventEmitter } from '@angular/core';
import { generoCreacionDTO } from '../genero';

import { primeraletraMayuscula } from 'src/app/utilidades/rating/validadores/primera_letra_mayuscula';

@Component({
  selector: 'app-formulario-genero',
  templateUrl: './formulario-genero.component.html',
  styleUrls: ['./formulario-genero.component.css']
})
export class FormularioGeneroComponent implements OnInit {

  constructor(private formBuilder: FormBuilder) { }

  form:FormGroup
  @Input()
  modelo: generoCreacionDTO;
  @Output()
  submit: EventEmitter<generoCreacionDTO> = new EventEmitter<generoCreacionDTO>();

  ngOnInit(): void {
    this.form= this.formBuilder.group({
      nombre:['', {validators: [Validators.required, Validators.minLength(3),primeraletraMayuscula()]}]
    });
    if(this.modelo !== undefined){
      this.form.patchValue(this.modelo);
    }
  }
  Guardar_genero(){
    this.submit.emit(this.form.value);
  }
  
  obtenerErrorNombre(){
    var campo= this.form.get('nombre');

    if(campo.hasError('required')){
      return 'El campo nombre es requerido';
    }

    if(campo.hasError('minlength')){
      return 'Minimo de letras permitada es 3';
    }
    if(campo.hasError("primeraletraMayuscula")){
      return campo.getError("primeraletraMayuscula").mensaje;
    }
  }
}
