import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { primeraletraMayuscula } from 'src/app/utilidades/rating/validadores/primera_letra_mayuscula';
import { generoCreacionDTO } from '../genero';

@Component({
  selector: 'app-crear-genero',
  templateUrl: './crear-genero.component.html',
  styleUrls: ['./crear-genero.component.css']
})
export class CrearGeneroComponent{

  constructor(private router: Router ) { }

  Guardar_genero(genero: generoCreacionDTO){
    console.log(genero)
    this.router.navigate(['/generos']);
   }
}